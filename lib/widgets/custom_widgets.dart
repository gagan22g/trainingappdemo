import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:my_trainings_app/App%20Style/app_colors.dart';
import 'package:my_trainings_app/Models/horizontal_item.dart';
import 'package:my_trainings_app/providers/home_provider.dart';

class CustomWidgets {
  Widget horizontalListItem({
    required double width,
    required String title,
    required String subTitle,
    required int price,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Container(
        width: width,
        decoration: BoxDecoration(
            image: DecorationImage(
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.5), BlendMode.dstOver),
                image: const AssetImage("assets/training_banner.png"),
                fit: BoxFit.cover)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                title,
                style: TextStyles.textTheme(
                    fontColor: Colors.white,
                    fontSize: 18,
                    weight: FontWeight.bold),
              ),
              Text(
                subTitle,
                style: TextStyles.textTheme(
                    fontColor: Colors.white,
                    fontSize: 15,
                    weight: FontWeight.normal),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: "\$600",
                          style: TextStyles.crossTextTheme(
                              fontColor: Colors.red,
                              fontSize: 10,
                              decorationColor: Colors.red,
                              weight: FontWeight.normal),
                        ),
                        TextSpan(
                          text: " \$${price.toString()}",
                          style: TextStyles.textTheme(
                              fontColor: Colors.red,
                              fontSize: 16,
                              weight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                            text: "View Details ",
                            style: TextStyles.textTheme(
                                fontColor: Colors.white,
                                fontSize: 14,
                                weight: FontWeight.normal)),
                        const WidgetSpan(
                          child: Icon(Icons.arrow_forward_outlined,
                              size: 14, color: Colors.white),
                        ),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget nextPreviousButton(
      {required bool isLeft,
      required double width,
      required Function onClick}) {
    return InkWell(
      child: Container(
        color: Colors.black12.withOpacity(0.2),
        height: 80,
        width: width,
        alignment: Alignment.center,
        child: Icon(
          isLeft ? Icons.chevron_left_sharp : Icons.chevron_right_sharp,
          color: Colors.white,
        ),
      ),
      onTap: () => onClick(),
    );
  }

  Widget filterButton({required Function onClick}) {
    return InkWell(
        onTap: () => onClick(),
        child: Container(
          padding: const EdgeInsets.all(5),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(5),
            border: Border.all(width: 1.0, color: Colors.grey),
          ),
          child: RichText(
            text: TextSpan(
              children: [
                const WidgetSpan(
                  child: Icon(Icons.settings_input_component_rounded,
                      size: 14, color: Colors.grey),
                ),
                TextSpan(
                    text: "  Filter",
                    style: TextStyles.textTheme(
                        fontColor: Colors.grey,
                        fontSize: 14,
                        weight: FontWeight.normal)),
              ],
            ),
          ),
        ));
  }

  Widget verticalListItem(TrainingModel model) {
    return Card(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Container(
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            Expanded(
              flex: 4,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Oct 11-13, 2019",
                    style: TextStyles.textTheme(
                        fontColor: Colors.black,
                        fontSize: 15,
                        weight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    "8.30 am-12.30pm",
                    style: TextStyles.textTheme(
                        fontColor: Colors.black,
                        fontSize: 10,
                        weight: FontWeight.normal),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    model.location ?? '',
                    style: TextStyles.textTheme(
                        fontColor: Colors.black,
                        fontSize: 12,
                        weight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            const Expanded(
              flex: 1,
              child: DottedLine(
                dashLength: 3,
                dashGapLength: 3,
                lineThickness: 2,
                dashColor: Colors.black26,
                dashGapColor: Colors.white,
                direction: Axis.vertical,
                lineLength: 140,
              ),
            ),
            Expanded(
              flex: 8,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Filling Fast!",
                    style: TextStyles.textTheme(
                        fontColor: AppColors.appPrimaryColor,
                        fontSize: 11,
                        weight: FontWeight.w500),
                  ),
                  Text(
                    model.title ?? '',
                    style: TextStyles.textTheme(
                        fontColor: Colors.black,
                        fontSize: 15,
                        weight: FontWeight.bold),
                  ),
                  ListTile(
                    contentPadding: EdgeInsets.zero,
                    horizontalTitleGap: 5,
                    title: Text(
                      model.trainer ?? '',
                      style: TextStyles.textTheme(
                          fontColor: Colors.black,
                          fontSize: 12,
                          weight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      "Helen Gribble",
                      style: TextStyles.textTheme(
                          fontColor: Colors.black45,
                          fontSize: 12,
                          weight: FontWeight.normal),
                    ),
                    leading: circularProfile(),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: SizedBox(
                      height: 30,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: AppColors.appPrimaryColor),
                          onPressed: () {},
                          child: Text(
                            "Enroll Now",
                            style:
                                TextStyles.textTheme(fontColor: Colors.white),
                          )),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget circularProfile() {
    return SizedBox(
      height: 40,
      width: 40,
      child: Stack(
        clipBehavior: Clip.none,
        fit: StackFit.expand,
        children: [
          const CircleAvatar(
              backgroundColor: AppColors.appPrimaryColor,
              child: Icon(
                Icons.person,
                size: 20,
                color: Colors.white,
              )),
          Positioned(
              bottom: -15,
              right: -15,
              child: RawMaterialButton(
                onPressed: () {},
                elevation: 1.0,
                fillColor: const Color(0xFFF5F6F9),
                constraints: const BoxConstraints(minHeight: 25, minWidth: 25),
                child: const Icon(
                  Icons.camera_alt_outlined,
                  size: 10,
                  color: Colors.blue,
                ),
                padding: EdgeInsets.zero,
                shape: const CircleBorder(),
              )),
        ],
      ),
    );
  }

  Widget showFilterDialog(
          {required Size size,
          required Function onClick,
          required HomeProvider provider,
          required BuildContext ctx}) =>
      Container(
        height: size.height * 0.7,
        width: size.width,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    "Sort and Filters",
                    style: TextStyle(
                        fontSize: 23,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                  IconButton(
                      onPressed: () => onClick(),
                      icon: const Icon(Icons.close, color: Colors.grey)),
                ],
              ),
            ),
            const Divider(
              height: 2,
              color: Colors.black12,
              thickness: 2,
            ),
            Expanded(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 4,
                    child: ListView.builder(
                      padding: EdgeInsets.zero,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: provider.getListFilter.length,
                      itemBuilder: (ctx, index) {
                        return InkWell(
                          onTap: () => provider
                              .selectCategory(provider.getListFilter[index]),
                          child: Container(
                            width: size.width * 0.3,
                            height: 60,
                            color: !provider.getListFilter[index].isSelected!
                                ? Colors.black12
                                : Colors.white,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                provider.getListFilter[index].isSelected!
                                    ? const VerticalDivider(
                                        color: Colors.red,
                                        thickness: 10,
                                        width: 10,
                                      )
                                    : const SizedBox(width: 10),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 15),
                                    child: Text(
                                      provider.getListFilter[index].title ?? "",
                                      style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                      textAlign: TextAlign.start,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  Expanded(
                    flex: 7,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              padding: const EdgeInsets.all(5),
                              alignment: Alignment.centerLeft,
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                    width: 1.0,
                                    color: Colors.grey.withOpacity(0.6)),
                              ),
                              child: TextField(
                                  cursorColor: Colors.black,
                                  textAlign: TextAlign.left,
                                  decoration: InputDecoration(
                                    prefixIcon: const Icon(Icons.search,
                                        color: Colors.grey, size: 20),
                                    border: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    errorBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    contentPadding: EdgeInsets.zero,
                                    hintText: "Search",
                                    hintStyle: TextStyles.textTheme(
                                        fontSize: 16,
                                        fontColor:
                                            Colors.grey.withOpacity(0.6)),
                                  ),
                                  textAlignVertical: TextAlignVertical.center,
                                  style: TextStyles.textTheme(
                                    fontSize: 16,
                                    fontColor: Colors.black,
                                  ))),
                        ),
                        Expanded(
                          child: ListView.builder(
                            padding: EdgeInsets.zero,
                            itemCount: provider.getFilterItems.length,
                            itemBuilder: (ctx, index) {
                              return CheckboxListTile(
                                title: Text(
                                  provider.getFilterItems[index].title ?? "",
                                  style: TextStyles.textTheme(
                                    fontColor: Colors.black,
                                    fontSize: 13,
                                  ),
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                value:
                                    provider.getFilterItems[index].isSelected,
                                onChanged: (val) {
                                  provider.changeClick(
                                      provider.getFilterItems[index], val!);
                                },
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      );
}
