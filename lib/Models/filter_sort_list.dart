enum Category {
  sortBy,
  location,
  training,
  trainer,
}

class SortFilterModel {
  String? title;
  bool? isSelected;
  Category? category;

  SortFilterModel({
    this.title,
    this.isSelected,
    this.category,
  });

  SortFilterModel.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    isSelected = json['isSelected'];
    category = json['category'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = title;
    data['isSelected'] = isSelected;
    data['category'] = category;
    return data;
  }
}
