class TrainingModel {
  String? title;
  String? subTitle;
  int? price;
  String? location;
  String? trainer;

  TrainingModel(
      {this.title, this.subTitle, this.price, this.location, this.trainer});

  TrainingModel.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    subTitle = json['subTitle'];
    price = json['price'];
    location = json['location'];
    trainer = json['trainer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = title;
    data['subTitle'] = subTitle;
    data['price'] = price;
    data['location'] = location;
    data['trainer'] = trainer;
    return data;
  }
}
