import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_trainings_app/App%20Style/app_colors.dart';
import 'package:my_trainings_app/Screens/home_screen.dart';
import 'package:my_trainings_app/Screens/navigator.dart';
import 'package:my_trainings_app/providers/home_provider.dart';
import 'package:provider/provider.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.black,
      statusBarBrightness: Brightness.light,
      statusBarIconBrightness:
          Brightness.light //or set color with: Color(0xFF0000FF)
      ));

  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider.value(value: HomeProvider())],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'My Training App',
          theme: ThemeData(
            primaryColor: AppColors.appPrimaryColor,
          ),
          home: const HomeScreen(),
          routes: {
            Navigationdrawer.routeName: (ctx) => const Navigationdrawer(),
          }),
    );
  }
}
