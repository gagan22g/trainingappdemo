import 'package:flutter/material.dart';

abstract class AppColors {
  static const appPrimaryColor = Color(0xffFF4855);
}

bool isTablet(double shortestSide) => shortestSide >= 600;

class TextStyles {
  static bool _isTablet = false;

  static void setIstablet(BuildContext context) {
    _isTablet = isTablet(MediaQuery.of(context).size.shortestSide);
  }

  static TextStyle textTheme({
    FontWeight? weight,
    double? fontSize,
    Color? fontColor,
  }) {
    return TextStyle(
        fontWeight: weight ?? FontWeight.normal,
        fontStyle: FontStyle.normal,
        fontSize: fontSize ?? 14.0 * (TextStyles._isTablet ? 1.5 : 1),
        color: fontColor ?? AppColors.appPrimaryColor);
  }

  static TextStyle crossTextTheme(
      {FontWeight? weight,
      double? fontSize,
      Color? fontColor,
      Color? decorationColor}) {
    return TextStyle(
        fontWeight: weight ?? FontWeight.normal,
        fontStyle: FontStyle.normal,
        decoration: TextDecoration.lineThrough,
        decorationColor: decorationColor,
        fontSize: fontSize ?? 14.0 * (TextStyles._isTablet ? 1.5 : 1),
        color: fontColor ?? AppColors.appPrimaryColor);
  }
}
