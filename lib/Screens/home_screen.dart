import 'package:flutter/material.dart';
import 'package:my_trainings_app/App%20Style/app_colors.dart';
import 'package:my_trainings_app/Models/horizontal_item.dart';
import 'package:my_trainings_app/Screens/navigator.dart';
import 'package:my_trainings_app/providers/home_provider.dart';
import 'package:my_trainings_app/widgets/custom_widgets.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late PageController _pageCtr;

  @override
  void initState() {
    _pageCtr = PageController(initialPage: 0); //Default page 0
    Provider.of<HomeProvider>(context, listen: false)
        .init(); //Intialize all list with dummy data
    super.initState();
  }

  //To changing page with animation
  _animateToIndex(i) => _pageCtr.animateToPage(i,
      duration: const Duration(milliseconds: 300), curve: Curves.easeIn);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.grey,
      body: SafeArea(
        child: Column(
          children: [
            Container(
              height: size.height * 0.5,
              color: Colors.white,
              child: Stack(
                children: [
                  Container(
                    color: AppColors.appPrimaryColor,
                    height: size.height * 0.32,
                    width: size.width,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Trainings",
                            style: TextStyles.textTheme(
                                weight: FontWeight.bold,
                                fontColor: Colors.white,
                                fontSize: 30),
                          ),
                          IconButton(
                              onPressed: () => Navigator.pushNamed(
                                  context, Navigationdrawer.routeName),
                              icon: const Icon(
                                Icons.menu,
                                color: Colors.white,
                              )),
                        ],
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                          left: 16.0,
                          right: 16,
                          top: size.height * 0.13,
                          bottom: 20,
                        ),
                        child: Text(
                          "Highlights",
                          style: TextStyles.textTheme(
                              weight: FontWeight.w500,
                              fontColor: Colors.white,
                              fontSize: 20),
                        ),
                      ),
                      Consumer<HomeProvider>(builder: (ctx, provider, _) {
                        return SizedBox(
                          width: size.width,
                          height: size.height * 0.2,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CustomWidgets().nextPreviousButton(
                                  isLeft: true,
                                  width: size.width * 0.06,
                                  onClick: () {
                                    if (provider.pos > 0) {
                                      _animateToIndex(--provider.pos);
                                    }
                                  }),
                              Expanded(
                                child: PageView.builder(
                                  controller: _pageCtr,
                                  itemCount: provider.getList.length,
                                  onPageChanged: (int value) {
                                    provider.changePos(value);
                                  },
                                  itemBuilder: (ctx, pos) {
                                    return CustomWidgets().horizontalListItem(
                                      width: size.width * 0.83,
                                      title: provider.getList[pos].title ?? '',
                                      subTitle:
                                          provider.getList[pos].subTitle ?? '',
                                      price: provider.getList[pos].price ?? 0,
                                    );
                                  },
                                  physics: const NeverScrollableScrollPhysics(),
                                ),
                              ),
                              CustomWidgets().nextPreviousButton(
                                  isLeft: false,
                                  width: size.width * 0.06,
                                  onClick: () {
                                    _animateToIndex(++provider
                                        .pos); //to change page with animation
                                  }),
                            ],
                          ),
                        );
                      }),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 15),
                        child: CustomWidgets().filterButton(onClick: () {
                          showModalBottomSheet(
                              context: context,
                              backgroundColor: Colors.black.withOpacity(0.2),
                              elevation: 10,
                              enableDrag: false,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              isScrollControlled: true,
                              builder: (BuildContext ctxt) {
                                return Consumer<HomeProvider>(
                                    //Consumer for updating filter
                                    builder: (ctx, provider, _) {
                                  return CustomWidgets().showFilterDialog(
                                      size: size,
                                      onClick: () =>
                                          Navigator.maybePop(context),
                                      provider: provider,
                                      ctx: context);
                                });
                              });
                        }),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                child: Consumer<HomeProvider>(builder: (context, provider, _) {
                  return ListView.builder(
                    padding: EdgeInsets.zero,
                    itemCount: provider.getVerticalList.length,
                    itemBuilder: (context, index) {
                      return CustomWidgets()
                          .verticalListItem(provider.getVerticalList[index]);
                    },
                  );
                }),
              ),
            )
          ],
        ),
      ),
    );
  }
}
