import 'package:flutter/material.dart';
import 'package:my_trainings_app/App%20Style/app_colors.dart';

class Navigationdrawer extends StatelessWidget {
  static const routeName = "/navigation";
  const Navigationdrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.appPrimaryColor,
        leading: IconButton(
            onPressed: () => Navigator.maybePop(context),
            icon: const Icon(Icons.arrow_back, color: Colors.white)),
      ),
      backgroundColor: Colors.white,
    );
  }
}
