import 'package:flutter/cupertino.dart';
import 'package:my_trainings_app/Models/filter_sort_list.dart';
import 'package:my_trainings_app/Models/horizontal_item.dart';

class HomeProvider extends ChangeNotifier {
  late List<TrainingModel> _list;
  late List<TrainingModel> _verticalList;
  late List<SortFilterModel> _sFlist;
  late List<SortFilterModel> _locList;
  late List<SortFilterModel> _tNameList;
  late List<SortFilterModel> _trainerList;
  late int pos;

  Future<void> init() async {
    pos = 0;
    _list = [];
    _verticalList = [];
    _sFlist = [];
    _locList = [];
    _tNameList = [];
    _trainerList = [];
    _list.add(TrainingModel(
        title: "Sale Scrum Master",
        subTitle: "West Des Moines, IA-30 Oct-31 Oct",
        price: 825));
    _list.add(TrainingModel(
        title: "Sale Scrum",
        subTitle: "West Des Moines, IA-30 Nov-31 Nov",
        price: 625));
    _list.add(TrainingModel(
        title: "Sale Scrum Master",
        subTitle: "West Des Moines, IA-30 Dec-31 Dec",
        price: 725));

    //Verical List
    _verticalList.add(TrainingModel(
        title: "CSS Training",
        subTitle: "West Des Moines, IA-30 Oct-31 Oct",
        location: "West Des Moines",
        trainer: "James",
        price: 825));
    _verticalList.add(TrainingModel(
        title: "Dart Training",
        subTitle: "West Des Moines, IA-30 Nov-31 Nov",
        location: "West Des Moines",
        trainer: "",
        price: 625));
    _verticalList.add(TrainingModel(
        title: "APP Training",
        subTitle: "West Des Moines, IA-30 Dec-31 Dec",
        location: "Chicago, IL",
        trainer: "Chris Lyon",
        price: 725));
    _verticalList.add(TrainingModel(
        title: "APP Training",
        subTitle: "West Des Moines, IA-30 Oct-31 Oct",
        location: "Phonenix, XZ",
        trainer: "Mark Zack",
        price: 825));
    _verticalList.add(TrainingModel(
        title: "CSS Training",
        subTitle: "West Des Moines, IA-30 Nov-31 Nov",
        location: "Dallas,TX",
        trainer: "John Phillips",
        price: 625));

    _sFlist.add(SortFilterModel(
        title: "Sort by", isSelected: true, category: Category.sortBy));
    _sFlist.add(SortFilterModel(
        title: "Location", isSelected: false, category: Category.location));
    _sFlist.add(SortFilterModel(
        title: "Training Name",
        isSelected: false,
        category: Category.training));
    _sFlist.add(SortFilterModel(
        title: "Trainer", isSelected: false, category: Category.trainer));

    //  Location names
    _locList.add(SortFilterModel(
        title: "West Des Moines",
        isSelected: false,
        category: Category.location));
    _locList.add(SortFilterModel(
        title: "Chicago, IL", isSelected: false, category: Category.location));
    _locList.add(SortFilterModel(
        title: "Phonenix, XZ", isSelected: false, category: Category.location));
    _locList.add(SortFilterModel(
        title: "Dallas,TX", isSelected: false, category: Category.location));

    //  Training Names
    _tNameList.add(SortFilterModel(
        title: "CSS Training", isSelected: false, category: Category.training));
    _tNameList.add(SortFilterModel(
        title: "Dart Training",
        isSelected: false,
        category: Category.training));
    _tNameList.add(SortFilterModel(
        title: "APP Training", isSelected: false, category: Category.training));
    _tNameList.add(SortFilterModel(
        title: "HTML Training",
        isSelected: false,
        category: Category.training));

    // Trainer Names
    _trainerList.add(SortFilterModel(
        title: "James", isSelected: false, category: Category.trainer));
    _trainerList.add(SortFilterModel(
        title: "John Phillips", isSelected: false, category: Category.trainer));
    _trainerList.add(SortFilterModel(
        title: "Mark Zack", isSelected: false, category: Category.trainer));
    _trainerList.add(SortFilterModel(
        title: "Chris Lyon", isSelected: false, category: Category.trainer));
  }

  changePos(int i) {
    pos = i;
    notifyListeners();
  }

  int get getPos => pos;
  List<TrainingModel> get getList => [..._list].toList();
  List<TrainingModel> get getVList => [..._verticalList].toList();
  List<SortFilterModel> get getListFilter => [..._sFlist].toList();
  List<SortFilterModel> get getFilterItems {
    var model = _sFlist.firstWhere((element) => element.isSelected!);
    switch (model.category) {
      case Category.sortBy:
        return [];
      case Category.location:
        return [..._locList].toList();
      case Category.training:
        return [..._tNameList].toList();
      case Category.trainer:
        return [..._trainerList].toList();
      default:
        return [];
    }
  }

  void changeClick(SortFilterModel model, bool val) {
    switch (model.category) {
      case Category.sortBy:
        break;
      case Category.location:
        _locList
            .firstWhere((element) => element.title == model.title)
            .isSelected = val;

        notifyListeners();
        break;
      case Category.training:
        _tNameList
            .firstWhere((element) => element.title == model.title)
            .isSelected = val;

        notifyListeners();
        break;
      case Category.trainer:
        _trainerList
            .firstWhere((element) => element.title == model.title)
            .isSelected = val;

        notifyListeners();
        break;
      default:
        break;
    }
  }

  List<TrainingModel> get getVerticalList {
    List<TrainingModel> _selList = [];
    var sLocList = _locList.where((element) => element.isSelected!);
    var sTrainerList = _trainerList.where((element) => element.isSelected!);
    var sTrainingList = _tNameList.where((element) => element.isSelected!);
    if (sLocList.isNotEmpty) {
      for (var item in sLocList) {
        _selList.addAll(
            _verticalList.where((element) => element.location == item.title));
      }
    }
    if (sTrainerList.isNotEmpty) {
      for (var item in sTrainerList) {
        _selList.addAll(
            _verticalList.where((element) => element.trainer == item.title));
      }
    }
    if (sTrainingList.isNotEmpty) {
      for (var item in sTrainingList) {
        _selList.addAll(
            _verticalList.where((element) => element.title == item.title));
      }
    }

    return _selList.isEmpty ? [..._verticalList].toList() : _selList.toList();
  }

  void selectCategory(SortFilterModel model) {
    for (var element in _sFlist) {
      if (element == model) {
        element.isSelected = true;
      } else {
        element.isSelected = false;
      }
      notifyListeners();
    }
  }
}
